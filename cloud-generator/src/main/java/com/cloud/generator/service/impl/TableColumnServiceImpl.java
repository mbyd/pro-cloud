/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.generator.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.generator.entity.TableColumn;
import com.cloud.generator.mapper.TableColumnMapper;
import com.cloud.generator.service.TableColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字段
 * @author Aijm
 * @since 2020/5/14
 */
@Service
public class TableColumnServiceImpl extends ServiceImpl<TableColumnMapper, TableColumn> implements TableColumnService {

    @Autowired
    private TableColumnMapper tableColumnMapper;

    /**
     * 获取数据表字段
     *
     * @param tableName
     * @return
     */
    @Override
    public List<TableColumn> getTableColumnList(String tableName) {
        return tableColumnMapper.getTableColumnList(tableName);
    }

    /**
     * 查询表备注信息
     *
     * @param tableName
     * @return
     */
    @Override
    public String queryTableInfo(String tableName) {
        return tableColumnMapper.queryTableInfo(tableName);
    }
}