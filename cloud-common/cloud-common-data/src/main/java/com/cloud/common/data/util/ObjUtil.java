/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.util;


import com.cloud.common.data.user.SystemService;
import com.cloud.common.entity.BaseEntity;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

/**
 * 对象操作
 * @author Aijm
 * @since 2019/8/24
 */
@UtilityClass
public class ObjUtil {


    private final SystemService systemService = SpringUtil.getBean(SystemService.class);

    /**
     * 插入之前执行方法，需要手动调用
     */
    public <T extends BaseEntity> void preInsert(T entity){

        // 用户id
        Long userId = entity.getCreateBy()!=null ? entity.getCreateBy() : systemService.getUserId();

        entity.setCreateBy(userId);
        entity.setUpdateBy(userId);
        entity.setId(IdUtils.getNextId());
        LocalDateTime now = LocalDateTime.now();
        entity.setCreateDate(now);
        entity.setUpdateDate(now);
        entity.setDelFlag(T.DEL_FLAG_NORMAL);
    }
    

    /**
     * 更新之前执行方法，需要手动调用
     */
    public <T extends BaseEntity> void preUpdate(T entity){
        Long userId = entity.getUpdateBy()!=null ? entity.getUpdateBy() : systemService.getUserId();
        entity.setUpdateBy(userId);
        LocalDateTime now = LocalDateTime.now();
        entity.setUpdateDate(now);
    }


}