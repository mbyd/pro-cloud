/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.aspect;

import com.alibaba.fastjson.JSONObject;
import com.cloud.common.data.entity.SysLog;
import com.cloud.common.data.user.SystemService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Author Aijm
 * @Description 普通日志切面
 * @Date 2020/5/18
 */
@Aspect
@AllArgsConstructor
@Slf4j
@Component
public class AspectLog {

    private final SystemService systemService;

    @Pointcut("execution (* com.cloud.*.service.impl.*Impl.*(..))")
    private void pointcut() {
    }

    @SneakyThrows
    @Around("pointcut()")
    public Object saveSysLog(ProceedingJoinPoint point) {

        // 请求的参数
        String params = Arrays.toString(point.getArgs());
        SysLog sysLog = SysLog.builder()
                .content(params)
                .userId(systemService.getUserId())
                .build();
        // 处理时间
        Long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        Long endTime = System.currentTimeMillis();
        sysLog.setTime(endTime - startTime);
        sysLog.setResult(JSONObject.toJSONString(obj));
        log.info("切面日志:{}", JSONObject.toJSONString(sysLog));
        return obj;
    }

}