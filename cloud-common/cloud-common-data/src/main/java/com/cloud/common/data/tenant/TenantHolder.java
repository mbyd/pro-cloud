/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.tenant;

import lombok.experimental.UtilityClass;

/**
 * @Author Aijm
 * @Description 租户对dubbo调用处理
 * @Date 2020/6/11
 */
@UtilityClass
public class TenantHolder {

	/**
	 * 异常时数据量会添加
	 */
	private final ThreadLocal<String> TENANT_NAME = new ThreadLocal<>();

	public void setTenantId(String tenantId) {
		TENANT_NAME.set(tenantId);
	}

	public String getTenantId() {
		return TENANT_NAME.get();
	}

	public void clearTenantId() {
		TENANT_NAME.remove();
	}
}