/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.security.util;

import cn.hutool.core.util.StrUtil;
import com.cloud.common.data.util.ServletUtil;
import com.cloud.common.security.component.SecurityUser;
import com.cloud.common.util.var.StaticVar;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @Author Aijm
 * @Description 根据SecurityContext 获取信息
 * @Date 2020/3/26
 */
@Slf4j
public class SecurityUtil {


    /**
     * 判断该用户是不是超级管理员 并给admin赋值
     * @return
     */
    public static boolean hasAdmin(){
        Integer userType = getUserType();
        return StaticVar.DEFAULT_USERTYPE_ADMIN.equals(userType);
    }


    /**
     * 判断该用户是否登录
     * @return
     */
    public static boolean hasAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().isAuthenticated() && !SecurityContextHolder.getContext().getAuthentication().getName().
                equals("anonymousUser");
    }

    /**
     * 获取登录用户的信息
     * @return
     */
    public static Long getUserId(){
        String userId = RpcContext.getContext().getAttachment(StaticVar.USERID_ID);
        return StrUtil.isBlank(userId)? null: Long.valueOf(userId);
    }


    /**
     * 获取登录用户的类型
     * @return
     */
    public static Integer getUserType(){
        String userType = RpcContext.getContext().getAttachment(StaticVar.USER_TYPE);
        return StrUtil.isBlank(userType)? null: Integer.valueOf(userType);
    }


    /**
     * 获取登录用户的本身租户
     * @return
     */
    public static Integer getTenantId(){
        String tenantId = RpcContext.getContext().getAttachment(StaticVar.TENANT_ID);
        return StrUtil.isBlank(tenantId)? StaticVar.TENANT_ID_DEFAULT : Integer.valueOf(tenantId);
    }


    /**
     * 获取登录用户的信息 登录名
     * @return
     */
    public static String getUserName(){
        return RpcContext.getContext().getAttachment(StaticVar.USER_NAME);
    }



    ////////////////////////////////  下列方法不再建议使用 ///////////////////////////////
    /**
     * 获取SecurityUser
     * @return
     */
    private static SecurityUser getSecurityUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        return (SecurityUser)authentication.getPrincipal();
    }

    /**
     * 获取登录用户的信息 登录名
     * @return
     */
    public static String getSecurityUserName(){
        try {
            return getSecurityUser().getUsername();
        } catch (Exception e) {
            log.info("没有登录", e);
        }
        return null;
    }

    /**
     * 获取登录用户的管理租户信息,(除了systemService,其他不在建议使用)
     * @return
     */
    public static Integer getSecurityTenantId(){
        if (SecurityUtil.hasAuthenticated()) {
            return getSecurityUser().getTenantId();
        }
        String tenantId = ServletUtil.getHeaderTenantId();
        return StrUtil.isBlank(tenantId)? StaticVar.TENANT_ID_DEFAULT: Integer.valueOf(tenantId);
    }

    /**
     * 获取登录用户的类型
     * @return
     */
    public static Integer getSecurityUserType(){
        try {
            return getSecurityUser().getUserType();
        } catch (Exception e) {
            log.info("没有登录", e);
        }
        return null;
    }

    /**
     * 获取用户id (只需要一个地方引用,其他地方不建议使用)
     * @return
     */
    public static Long getSecurityUserId(){
        try {
            return getSecurityUser().getUserId();
        } catch (Exception e) {
            log.info("没有登录! 默认用户id");
        }
        return null;
    }
}