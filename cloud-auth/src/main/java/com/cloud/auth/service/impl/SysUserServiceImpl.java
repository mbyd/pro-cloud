/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.service.impl;



import com.cloud.auth.entity.SysUser;
import com.cloud.auth.mapper.SysUserMapper;
import com.cloud.auth.service.SysUserService;
import com.cloud.common.data.base.BaseService;
import com.cloud.common.data.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


/**
 * 用户表
 *   主要为了登录信息
 * @author Aijm
 * @date 2019-08-25 20:20:58
 */
@Service
public class SysUserServiceImpl extends BaseService<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     *  根据账户或者邮箱登录 (不包含手机号登录)
     * @param loginName
     * @return
     */
    @Override
    public SysUser loginByName(String loginName) {
        return sysUserMapper.loginByName(loginName);
    }

    @Override
    public SysUser loginByPhone(String mobile) {
        return sysUserMapper.loginByPhone(mobile);
    }

    /**
     * QQ 登录
     *
     * @param qqOpenid
     * @return
     */
    @Override
    public SysUser loginByQQ(String qqOpenid) {
        return sysUserMapper.loginByQQ(qqOpenid);
    }

    /**
     * 微信登录
     *
     * @param wxOpenid
     * @return
     */
    @Override
    public SysUser loginByWX(String wxOpenid) {
        return sysUserMapper.loginByWX(wxOpenid);
    }

    /**
     * 登录后的操作
     *
     * @param loginName
     */
    @Override
    public void updateByLogin(String loginName) {
        LocalDateTime now = LocalDateTime.now();
        // 封装查询条件 和修改值
        SysUser sysUser = new SysUser();
        sysUser.setUpdateDate(now);
        sysUser.setLoginDate(now);
        sysUser.setLoginIp(ServletUtil.getRemoteAddr());

        sysUser.setLoginName(loginName);
        sysUserMapper.updateByLogin(sysUser);
    }


}