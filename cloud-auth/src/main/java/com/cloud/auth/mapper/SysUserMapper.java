/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.mapper;


import com.cloud.auth.entity.SysUser;
import com.cloud.common.data.base.ProMapper;

/**
 * 用户表
 *
 * @author Aijm
 * @date 2019-08-25 20:20:58
 */
public interface SysUserMapper extends ProMapper<SysUser> {

    /**
     *  根据账户或者邮箱登录 (不包含手机号登录)
     * @param loginName
     * @return
     */
    public SysUser loginByName(String loginName);

    /**
     *  根据手机号登录
     * @param mobile
     * @return
     */
    public SysUser loginByPhone(String mobile);


    /**
     * QQ 登录
     * @param qqOpenid
     * @return
     */
    public SysUser loginByQQ(String qqOpenid);


    /**
     * 微信登录
     * @param wxOpenid
     * @return
     */
    public SysUser loginByWX(String wxOpenid);


    /**
     * 根据登录名登录
     *
     * @param sysUser
     */
    public void updateByLogin(SysUser sysUser);



}