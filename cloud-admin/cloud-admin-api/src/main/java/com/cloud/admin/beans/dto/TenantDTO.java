/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.dto;

import com.cloud.admin.beans.po.OauthClientDetails;
import com.cloud.admin.beans.po.SysTenant;
import lombok.Data;

/**
 * 租户dto
 * @author Aijm
 * @since 2020/7/19
 */
@Data
public class TenantDTO extends SysTenant {
    /**
     * oauth_client_details 信息
     */
    private OauthClientDetails oauthClientDetails;


}