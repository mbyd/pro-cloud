/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.cloud.common.entity.TenantTreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author Aijm
 * @since 2019-05-13
 */
@Data
@Accessors(chain = true)
@TableName("sys_menu")
@ApiModel(value="Menu对象", description="菜单表")
public class SysMenu extends TenantTreeEntity<SysMenu> {

    private static final long serialVersionUID = -481808875271508064L;

    @ApiModelProperty(value = "菜单类型")
    private Integer type;

    @ApiModelProperty(value = "目标")
    @TableField(fill = FieldFill.UPDATE)
    private String target;

    @ApiModelProperty(value = "图标")
    @TableField(fill = FieldFill.UPDATE)
    private String icon;

    @ApiModelProperty(value = "是否在菜单中显示")
    @TableField(value="is_show", fill = FieldFill.UPDATE)
    private Integer hasShow;

    @ApiModelProperty(value = "权限标识")
    @TableField(fill = FieldFill.UPDATE)
    private String permission;

}