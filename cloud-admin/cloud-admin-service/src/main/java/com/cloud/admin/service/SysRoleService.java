/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.common.data.base.IProService;
import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.po.SysRole;
import com.cloud.admin.beans.po.SysUser;

import java.util.List;

/**
 * 角色表
 *
 * @author Aijm
 * @date 2019-08-25 20:57:31
 */
public interface SysRoleService extends IProService<SysRole> {

    /**
     * 根据用户 查询拥有的角色信息
     * @param sysUser
     * @return
     */
    List<RoleDTO> findList(SysUser sysUser);

    /**
     * 根据角色信息获取到菜单信息和角色信息
     * @param id
     * @return
     */
    RoleDTO get(Long id);

    /**
     * 更新角色信息
     * @param roleDTO
     * @return
     */
    boolean updateByRole(RoleDTO roleDTO);

    /**
     * 保存角色信息
     * @param roleDTO
     * @return
     */
    boolean saveRoleDTO(RoleDTO roleDTO);

    /**
     * 根据角色id 删除角色
     * @param id
     * @return
     */
    boolean removeRoleById(Long id);

}