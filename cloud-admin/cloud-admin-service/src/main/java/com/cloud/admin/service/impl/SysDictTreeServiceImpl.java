/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cloud.admin.beans.po.SysDictTree;
import com.cloud.admin.mapper.SysDictTreeMapper;
import com.cloud.admin.service.SysDictTreeService;
import com.cloud.common.cache.annotation.Cache;
import com.cloud.common.cache.annotation.CacheClear;
import com.cloud.common.cache.annotation.CacheConf;
import com.cloud.common.cache.constants.CacheScope;
import com.cloud.common.data.base.TreeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典表树
 *
 * @author Aijm
 * @date 2019-09-05 20:00:25
 */
@Service
@CacheConf(scope = CacheScope.DICT_TREE)
public class SysDictTreeServiceImpl extends TreeService<SysDictTreeMapper, SysDictTree> implements SysDictTreeService {



    @Override
    @Cache(key = "#typeCode", expire = 0)
    public List<SysDictTree> getDicTreeByType(String typeCode) {
        List<SysDictTree> list = super.list(Wrappers.<SysDictTree>query()
                .lambda().eq(SysDictTree::getTypeCode, typeCode)
                .orderByAsc(SysDictTree::getSort));
        return list;
    }

    @Override
    @CacheClear(key = "#entity.typeCode")
    public boolean save(SysDictTree entity) {
        return super.save(entity);
    }

    @Override
    @CacheClear(key = "#entity.typeCode")
    public boolean updateById(SysDictTree entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheClear(key = "#sysDictTree.typeCode")
    public boolean removeByDictTree(SysDictTree sysDictTree) {
        return super.removeById(sysDictTree.getId());
    }

}